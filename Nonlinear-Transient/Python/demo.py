from dolfin import *

# Solves the transient convection-diffusion-reaction problem with nonlinear reaction. 
# Setup Mesh
ny = 20
nx = 5*ny
mymesh = RectangleMesh(0.0, 0.0,10.0,2.0, nx, ny)
T = 0.1
dt = 0.0025
t = 0.0

u0 = Constant((0.0))
# Define convection and diffusion
diffusion = Expression("0.05+tanh(10.0*pow(x[0]-5.0,2) + 10.0*pow(x[1]-1,2))")
convection = Constant((-100.0, 0.0))

# Define QoI as average value over box in space and time
boxX = [1.0, 2.0]
boxY = [0.5, 1.5]
class MyExpression0(Expression):
    def __init__(self,boxX,boxY):
        self.boxX=boxX
        self.boxY=boxY
        self.area = (boxX[1]-boxX[0])*(boxY[1]-boxY[0])

    def eval(self,value, x): 
        if x[0] >= self.boxX[0] and x[0] <= self.boxX[1] and x[1] >= self.boxY[0] and x[1] <= self.boxY[1]:
            value[0] = 1.0/(self.area)
        else:
            value[0] = 0.0
psi = MyExpression0(boxX=boxX, boxY=boxY)


# Define Boundary Subdomains and BCS
class RightBoundary(SubDomain):  # define the Dirichlet boundary
    def inside(self, x, on_boundary):
        return x[0] > (10.0 - 5.0*DOLFIN_EPS) and on_boundary

class TBBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > DOLFIN_EPS and x[0] < (10.0 - DOLFIN_EPS) and on_boundary

RightBC = Expression("x[1]*(2.0-x[1])")
ZeroBC = Constant(0.0)

# Solve forward problem

# Define FE Space and BCs
V = FunctionSpace(mymesh, "Lagrange", 1)
bcr = DirichletBC(V, RightBC, RightBoundary())
bctb = DirichletBC(V, ZeroBC, TBBoundary())

# Define forward variational problem
u = Function(V)
v = TestFunction(V)
u_1 = interpolate(u0, V)
UList=[interpolate(u0,V)]
# Save solution in VTK format
file = File("plots/forward_solution.pvd")
u.assign(u_1)
u.rename("u", "Solution")
file << u
tList=[t]

# Perform Backward Euler Time Steps
while t < T:
    UList.append(Function(V))
    F = (u*v+dt*(dot(convection,grad(u))*v +diffusion*inner(grad(u), grad(v)) + u*(Constant(1.0)-u)*v)-u_1*v)*dx
    
    u.assign(u_1)
    t += dt
    tList.append(t)
    solve(F == 0, u, [bcr, bctb],  solver_parameters={"newton_solver":
                                                {"relative_tolerance": 1e-6,
                                                 'maximum_iterations': 50}})
    u_1.assign(u)
    UList[-1].assign(u_1)
    u.rename("u", "Solution")
    file << u

QoI =assemble(inner(u, psi)*dx) # Calculate QoI 

# Solve adjoint problem    
V2 = FunctionSpace(mymesh, "Lagrange", 2)
t = 0.0

# Define variational problem
bcr = DirichletBC(V2, ZeroBC, RightBoundary())
bctb = DirichletBC(V2, ZeroBC, TBBoundary())
u2 = Function(V2)
v2 = TestFunction(V2)
U2 = TrialFunction(V2)

# "Stiffness matrix" term
def a_K(w):
    return dot(-convection,grad(w))*v2*dx + diffusion*inner(nabla_grad(w), nabla_grad(v2))*dx + 2.0*w*v2*dx
# Right-Hand Side
def rhs(t):
    return (Constant(1.0))*v2*dx
# "Mass matrix" term
def a_M(w):
    return w*v2*dx

u_12 = interpolate(psi, V2)
U2List=[interpolate(psi,V2)]
# Save solution in VTK format
file = File("plots/adjoint_solution.pvd")
u2.assign(u_12)
u2.rename("u2", "Adjoint Solution")
file << u2
# Compute solution with Crank-Niccolson Time Steps
while t < T:
    t += dt
    u2 = Function(V2)
    U2List.append(Function(V2))
    a = a_M(U2) + 0.5*dt*a_K(U2)
    L = a_M(u_12) + 0.5*dt*(-a_K(u_12)+rhs(t-dt)+rhs(t))
    solve(a==L, u2,[bcr, bctb])
    u_12.assign(u2)
    U2List[-1].assign(u2)
    u2.rename("u2", "Adjoint Solution")
    file << u2

# Calculate Error Estimate
V3 = FunctionSpace(mymesh, 'Lagrange', 5)
errorCalculation = assemble(inner(u0-UList[0],U2List[-1])*dx) #discretization error

#errorList=[]
file = File("plots/errorID.pvd")

#calculate error at each time step
for i in range(1,len(tList)):
    # jump term
    term1 = (UList[i]-UList[i-1])*U2List[-i-1+1]
    U2 = -0.5*(U2List[-i-1+1] + U2List[-i-1])
    # reaction term
    term2 = -dt*UList[i]*(interpolate(Constant(1.0),V)-UList[i])*U2
    # diffusion term
    term3 = -dt*inner(diffusion*nabla_grad(UList[i]), nabla_grad(U2))
    # convection term
    term4 = -dt*dot(convection,grad(UList[i]))*U2

    errorID = term1+term2+term3+term4 #error identifier at step
    errorCalculation += assemble((errorID)*dx)
    #save error indicator
    errorSave = Function(V2)
    errorSave.assign(project(errorID,V))
    errorSave.rename("error indicator", "Error Indicator")
    file << errorSave

print "QoI Estimate: ", QoI   
print "Error Estimate: ", errorCalculation


    
