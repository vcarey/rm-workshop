function fg=qoibox(ACES,gp,tval,current_mesh,current_method,current_degree,current_qrule);

global xl xu yl yu;
%Defaults;  assumes if one is not defined all are not defined
if isempty(xl)
xl= 1.0;
xu= 3.0;
yl= .5;
yu= 1.5;
end

box_area=(xu-xl)*(yu-yl);
inv_box_area=1/box_area;
for i=1:length(gp)
	x=gp(i,1);
        y=gp(i,2);
if x >= xl && x <= xu && y>=yl && y<=yu
  fg(i)=inv_box_area;  %Box Area
else
  fg(i)=0; 
end
end
fg=fg';

	 
