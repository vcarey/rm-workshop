function setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   varargout = setup
% 
%   Adds all of the ACES paths to the MATLAB path.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IMPORTANT: MODIFY THE FOLLOWING ACES ROOT PATH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ACESroot = '~/ACES_12_30_10';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add all of the ACES folders to the path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath(strcat(ACESroot,'/geometries'), ...
	strcat(ACESroot,'/sample_work'), ...
	strcat(ACESroot,'/distmesh'), ...
	strcat(ACESroot,'/initialization'), ... 
	strcat(ACESroot,'/initialization/tools'), ... 
        strcat(ACESroot,'/initialization/DegreesOfFreedom'), ...
        strcat(ACESroot,'/initialization/Implementation'), ...
        strcat(ACESroot,'/initialization/Integration'), ...
        strcat(ACESroot,'/initialization/Integration/Quadrature'), ...
	strcat(ACESroot,'/postprocess'), ... 
	strcat(ACESroot,'/solvers'), ... 
	strcat(ACESroot,'/solvers/linear_solvers'), ... 
	strcat(ACESroot,'/solvers/nonlinear_solvers'), ... 
	strcat(ACESroot,'/solvers/time_solvers'), ... 
	strcat(ACESroot,'/solvers/tools')) 
















