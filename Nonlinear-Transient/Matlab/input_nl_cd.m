%initialize paths-change setup.m to point to your ACES directory
setup
[ACES] = useGeometry('long_box_shortcourse',.1); 

%% We write the nonlinear problem as an iteration using Newton's method
%%Linear Part Au
ACES.physics(1,1).a = inline('.05+tanh(10*(x-5)^2+10*(y-1)^2)');
ACES.physics(1,1).bx = -100;
%%Nonlinear Part F(u)
ACES.function.force = inline('u1*(1-u1)');
%% (A-F'(U_n))\delta=F(U_n)-AU_n; as U_{n+1}=U_n+\delta
%% (A-F'(U_n)U_{n+1}=(A-F'(U_n)(U_n)+(A-F'(U_n))\delta \rightarrow F(U_n)-AU_n
%% RHS for fixed point iteration is F(U_n))-F'(U_n)U_n
ACES.physics(1,1).c=inline('2*u1-1');
ACES.physics(1,1).f=inline('u1^2');
%% Time Derivative
ACES.physics(1,1).m=1;
%% Initial condition
ACES.physics(1,1).init=0;

%% Definitions for postprocessing
%ACES.function.force=ACES.physics(1,1).f;
ACES.function.diff=ACES.physics(1,1).a;
ACES.function.conv=ACES.physics(1,1).bx;



% Adjoint Physics
ACES.physics(2,2).a=ACES.physics(1,1).a
ACES.physics(2,2).bx=100;

%% End time QoI: Average Value at t=.1 on [1,.5] \times [3,1.5]
ACES.function.qoi=@qoibox;
global xl xu yl yu;



ACES.physics(2,2).init=ACES.function.qoi;
ACES.physics(2,2).c=inline('2*u1-1');
ACES.physics(2,2).m=1;

ACES.physics(1,1).boundary(1).value=inline('y*(2-y)');
ACES.physics(1,1).boundary(4).type='N';

ACES.physics(2,2).boundary(4).type='N';

%Time mesh is from t=0 to t=.1, 
ACES.timespan.mesh = 0:.0025:.1;

%setup the spatial solve-PL for U1, Quadratics for U2, higher-order gaussian quadrature on both equations
ACES = initialize(ACES,'degree',[1 2],'qrule',[3 3]);

%save the time history so we can compute the adjoint equation and error representation
ACES.settings.savesol=1;

%forward solve for U_1 with Backward Euler(DG0 + right endpoint in timequadrature)
ACES = solve(ACES,'solver','TD','whichvars',1,'max_time_iter',6,'timemethod',[2 1],'timedegree',[0 1],'timeqrule',[1 4]);

%Map to solution from [0,.01] to [.01,0]
ACES=time_map(ACES,1);


%solve for U_2 via Crank-Nicholson(CG1+trapezoidal rule in time quadrature)
ACES= solve(ACES,'solver','TD','whichvars',2,'timemethod',[2 1],'timedegree',[0 1],'timeqrule',[1 4]);

%Map both variables back to [0,.01]
ACES=time_map(ACES,1);
ACES=time_map(ACES,2);

%save the dgsolution
dgsol.timeinterval=ACES.solution(1).timeinterval;

%plot solutions for both variables
for i=0:.02:.1
FEMplot(ACES,'u','time',i);
FEMplot(ACES,'u2','time',i);
end

%construct continous time interpolant-NO GALERKIN ORTHOG
CGsol=projDGt(ACES,1);
ACES.solution(1).timeinterval=CGsol;
ACES.settings.timedegree(1)=1;
ACES.settings.method(1)=1;

%Error Representation Formula: two point gauss rule sufficient in time(timeqrule=6)
[ACES,I] = integrate(ACES,inline('force*u2-u1t*u2-diff*(u1x*u2x+u1y*u2y)-conv*u1x*u2')','overalltime',1,'timeqrule',6);

%Plot of error contributions
FEMplot(ACES,'expression');
sum(ACES.mesh.expression)

%Same formula using midpoint rule in time
[ACES,Imid] = integrate(ACES,inline('force*u2-u1t*u2-diff*(u1x*u2x+u1y*u2y)-conv*u1x*u2')','overalltime',1,'timeqrule',3);

sum(ACES.mesh.expression)

%FEMplot(ACES,'expression');

%NO GALERKIN ORTHOGONALITY AS WE PROJECTED THE SOLUTION
[ACES,IP] = integrate(ACES,inline('force*(u2-P1u2)-u1t*(u2-P1u2)-diff*(u1x*(u2x-P1u2x)+u1y*(u2y-P1u2y))-conv*u1x*(u2-P1u2)'),'overalltime',1,'timeqrule',6);

%FEMplot(ACES,'expression');

%[ACES,jump]=jump_term(ACES,1,2,'u1*u2');
%[ACES,jump2]=jump_term(ACES,1,2,'u1*P1u2',1);

