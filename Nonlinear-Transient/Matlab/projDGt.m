function timeinterval=ProjDG0CG1(ACES,projvar)

%quick hack
ACES.settings.whichvars=projvar;
temp=getInitialValues(ACES);
timeinterval(1).sol(:,1)=temp.currentsol(projvar).sol;
timeinterval(1).sol(:,2)=ACES.solution(projvar).timeinterval(1).sol;
for i=2:length(ACES.timespan.mesh)-1
	timeinterval(i).sol(:,1)=ACES.solution(projvar).timeinterval(i-1).sol;
	timeinterval(i).sol(:,2)=ACES.solution(projvar).timeinterval(i).sol;
end

