function ACES=time_map(ACES,varnum)
  
  k=varnum;
tinter=length(ACES.solution(k).timeinterval);
for n = 1:tinter
    tempsol(n).sol = ACES.solution(k).timeinterval(n).sol;
 end
for n = 1:tinter
    ACES.solution(k).timeinterval(n).sol = fliplr(tempsol(tinter-n+1).sol);
end
