
%# Matlab script to generate discrete error estimate at \f$u(x=.5)\f$ for the 2 point BVP
%# \f$u^\prime\prime=f(x)=e^{\alpha x)}\f$, \f$u(0)=u(1)=0\f$, solved using a three-point CFD stencil,
%# with a uniform grid of size \f$h=.05\f$.

%# \f$\alpha=0\f$ so RHS = 1;
%# Setup Computational grid on \f$[h,1-h]\f$.
alpha=0;
h=.05;
xval=(h:h:1-h)';
pts=length(xval);
%# Setup Forward Solution:
A=spalloc(pts,pts,3);
b=zeros(pts,1); 
u_sol=zeros(pts,1);
%# Discretize ODE: \f[ -u^\prime\prime(x_n)= \frac{2u(x_n)-u(x_{n-1})-u(x_{n+1})}{h^2} = e^{\alpha x_n} \f]

%# Uniform grid so grid size h can be moved to RHS
b=h^2*exp(alpha*xval);
%# We use the spdiags command to map -1 2 1 to the tridiagonal matrix A
temp=ones(pts,1);
A=spdiags([-temp 2*temp -temp],-1:1,A);

%# Create our approximate solution u_sol by 7 steps of CG(no preconditioner)
u_sol=pcg(A,b,[],7,[]);
%# Slash for "truth" solve.
u=A\b;

%# Now set up the adjoint problem and solve it("exactly").
%# QoI is u_sol(10), so \f$ \psi = e_{10}\f$.
psi=zeros(pts,1);
psi(10)=1;
phi=zeros(pts);

%# Atop=A'; Atop=A^\top not needed as A is symetric
%# Solve for adjoint solution using slash for "truth"
phi=A\psi;

%# Compute residual vector
Res=b-A*u_sol;
%# Estimated Error = \f$(R(U),\phi)\f$=(b-AU,\phi)=(b-AU)^\top\phi\f$.
err_est=Res'*phi;

%# Real Error u(10)-u_sol(10)
err=u(10)-u_sol(10);

%# Effectivity Index is Est/Err/
eff=err_est/err;

%# second QoI=Average value of u on [.6,.8]
psi2=zeros(pts,1);
psi2(12:16)=.2;
phi2=zeros(pts);

%# Adjoint matrix is the same

phi2=A\psi2;

%# Repeat Error block
err_est2=Res'*phi2;

err2=mean(u(12:16)-u_sol(12:16));

eff2=err_est2/err2;

%# Some plotting and discussion

plot(xval,u_sol,'b*',xval,u,'r-');
legend({'U approx','U'});

figure;
%# Influence functions: Adjoint solutions
plot(xval,phi,xval,phi2)
legend({'\phi_{.5}','\phi_{avr}'});

figure;
%# "Local Error Contributions"
plot(xval,u-u_sol,xval,Res.*phi,xval,Res.*phi2);








