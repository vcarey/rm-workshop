setup
[ACES] = useGeometry('long_box_shortcourse',0.25); 
%Omega = [0,10] X [0,2]

ACES.physics(1,1).a = inline('.05+tanh(10*(x-5)^2+10*(y-1)^2)');
ACES.physics(1,1).bx = -100; % -p^2
ACES.physics(1,1).f = inline('1');
ACES.function.force=ACES.physics(1,1).f;
ACES.function.diff=ACES.physics(1,1).a;
ACES.function.conv=ACES.physics(1,1).bx;
ACES.function.conv2=-20;

% Adjoint Physics
ACES.physics(2,2).a=ACES.physics(1,1).a
ACES.physics(2,2).bx=100;
ACES.physics(2,2).f=@qoibox;
% specify box for QoI (non-default)
global xl xu yl yu;
xl=5;
xu=7;
yl=1;
yu=2;

%for postprocessing tag the adjoint RHS as the QoI
ACES.function.qoi=ACES.physics(2,2).f;

%ACES DEFAULT BC ARE ZERO DIRICHLET
%BOUNDARY 1(X=10) PARABOLIC INFLOW
ACES.physics(1,1).boundary(1).value=inline('y*(2-y)');
%BOUNDARY 4(X=0) ZERO NEUMANN
ACES.physics(1,1).boundary(4).type='N';

%ADJOINT BC ARE THE SAME IN THIS CASE BUT ARE ZERO
ACES.physics(2,2).boundary(4).type='N';

[ACES] = initialize(ACES,'degree',[1 2],'qrule',[3 3]);
ACES = solve(ACES);

%ERROR REP FORMULA
[ACES,I] = integrate(ACES,inline('force*u2-diff*(u1x*u2x+u1y*u2y)-conv*u1x*u2'));


%SENS OF BETA AT B=10
[ACES,SP] = integrate(ACES,inline('-conv2*u1x*u2'));

[ACES] = initialize(ACES,'degree',[1 1],'qrule',[3 3]);
ACES = solve(ACES);

%ERROR REP FORMULA
[ACES,I] = integrate(ACES,inline('force*u2-diff*(u1x*u2x+u1y*u2y)-conv*u1x*u2'));


%SENS OF BETA AT B=10
[ACES,SP] = integrate(ACES,inline('-conv2*u1x*u2'));



