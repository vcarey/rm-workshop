setup
[ACES] = useGeometry('long_box_shortcourse',0.25); 
%Omega = [0,10] X [0,2]

ACES.physics(1,1).a = inline('.05+tanh(10*(x-5)^2+10*(y-1)^2)');
ACES.physics(1,1).bx = -100;
ACES.physics(1,1).f = inline('1');
ACES.function.force=ACES.physics(1,1).f;
ACES.function.diff=ACES.physics(1,1).a;
ACES.function.conv=ACES.physics(1,1).bx;

% Adjoint Physics
ACES.physics(2,2).a=ACES.physics(1,1).a
ACES.physics(2,2).bx=100;
ACES.physics(2,2).f=@qoibox;
% specify box for QoI (non-default)
global xl xu yl yu;
xl=5;
xu=7;
yl=1;
yu=2;

%for postprocessing tag the adjoint RHS as the QoI
ACES.function.qoi=ACES.physics(2,2).f;

%ACES DEFAULT BC ARE ZERO DIRICHLET
%BOUNDARY 1(X=10) PARABOLIC INFLOW
ACES.physics(1,1).boundary(1).value=inline('y*(2-y)');
%BOUNDARY 4(X=0) ZERO NEUMANN
ACES.physics(1,1).boundary(4).type='N';

%ADJOINT BC ARE THE SAME IN THIS CASE BUT ARE ZERO
ACES.physics(2,2).boundary(4).type='N';

[ACES] = initialize(ACES,'degree',[1 2],'qrule',[3 3]);
ACES = solve(ACES);

%PLOT FORWARD SOLUTION
FEMplot(ACES,'u');
%PLOT ADJOINT SOLUTION
FEMplot(ACES,'u2');

%ERROR REP FORMULA
[ACES,I] = integrate(ACES,inline('force*u2-diff*(u1x*u2x+u1y*u2y)-conv*u1x*u2'));
FEMplot(ACES,'expression');

%ERROR REP FORMULA WITH GALERKIN ORTHOG.
[ACES,IP] = integrate(ACES,inline('force*(u2-P1u2)-diff*(u1x*(u2x-P1u2x)+u1y*(u2y-P1u2y))-conv*u1x*(u2-P1u2)'));

%I=IP BUT DIFFERENT ERROR PLOTS
FEMplot(ACES,'expression');

%u2 as GREENS FUNCTION FOR QOI
[ACES,I2]=integrate(ACES,inline('force*u2'));
%Approximate value of QoI
[ACES,I3]=integrate(ACES,inline('qoi*u1'));

%Error Rep formula missing very small boundary error term-differnce between
%u(x=10) and Pu(x=10) weighted by adjoint solution.
[ACES,I5]=integrateBoundary(ACES,inline('(u1-(y*(2-y)))*u2x'),'bnum',1);


