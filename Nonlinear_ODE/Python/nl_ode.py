import numpy as np
import numpy.linalg as linalg
import matplotlib.pyplot as plt
# Predator-Prey model:  \dot{X}=\begin{array}{ll}{ \alpha & \beta x_2 \\ \gamma x_1 & \lambda \right] X

def pred_res(Xcur,Xold,delt,alpha,beta,tau,gamma):
    res=Xcur-Xold-delt*np.array([alpha*Xcur[0]-beta*Xcur[0]*Xcur[1], tau*Xcur[0]*Xcur[1]-gamma*Xcur[1]])
    print 'Current Residual Norm: %f'%(linalg.norm(res))
    return res

def pred_jac(X,alpha,beta,tau,gamma):
    jac = np.array([[alpha-beta*X[1],-beta*X[0]],[ tau*X[1],tau*X[0]-gamma]])
    return jac

# Forward-Model (Backwrd Euler (DG0)) 

# time mesh points and \delta t
delt=.01
time= np.arange(0.0, 10.0+delt, delt)

# reference solution (backward euler,10000 timesteps)
Xref=[2.000179198587852,1.000810454405660] # "truth" X(T=10)

# solution matrix X(2xtimesteps) for storing solution.
X=np.zeros((2,len(time)))
# Initial condition X(:,1)
P1=2;
P2=1;
X[:,0]=np.array([P1,P2])

# Model parameters
alpha=1.1
beta=1
tau=1
gamma=1.1

# tolerance for newton solve
tol=1E-6

# forward timestepping

Xcur=X[:,0]
for tstep in range(1,len(time)):
    Xold=Xcur
    # Newton loop
    res=pred_res(Xcur,Xold,delt,alpha,beta,tau,gamma)
    print 'Time Step: %d'%(tstep)
    while np.max(np.abs(res)) > tol:
        # compute Newton step
        jac=pred_jac(Xcur,alpha,beta,tau,gamma)
        newton=linalg.solve(np.eye(2)-delt*jac,-res)
        print 'Norm step: %f'%(linalg.norm(newton))
        Xcur=Xcur+newton
        res=pred_res(Xcur,Xold,delt,alpha,beta,tau,gamma)
        # import pdb
        # pdb.set_trace()
    
    # pdate and store X at new time value
    X[:,tstep]=Xcur


# Adjoint (Reverse) Solve:Crank-Nicholson(CG1)
Phi = np.zeros(X.shape)
# Adjoint Initial condition: QoI is Prey population at t=T(final)
Phi[:,-1]=np.array([1,0])
for tstep in range(len(time)-2,-1,-1):
     # Get Jacobian values at current time interval and assemble RHS
     RHS=Phi[:,tstep+1]+.5*delt*np.dot(pred_jac(X[:,tstep+1],alpha,beta,tau,gamma),Phi[:,tstep+1])
     # Get Jacobian values at new time and build matrix
     AMAT=np.eye(2)-delt*.5*pred_jac(X[:,tstep],alpha,beta,tau,gamma)
     # Solve for new time value
     Phi[:,tstep]=linalg.solve(AMAT,RHS)


# Error Contributions:
# No Initial Error (X(0),Phi(0))

# X is piecewise constant on a time interval and Phi is piecewise linear on a time interval
# Residual contribution on time interval with midpoint quadrature (-F(X),\Phi)_{T_i}

# Jump Error from DG=(X(t-)-X(t+),\Phi(2))
step_err=np.zeros(time.shape)
jump_err=np.zeros(time.shape)
step_err[0]=0.0
jump_err[0]=0.0
for tstep in range(2,len(time)):
   tres=[alpha*X[0,tstep]-beta*X[0,tstep]*X[1,tstep], -tau*X[0,tstep]*X[1,tstep]+gamma*X[1,tstep]]
   Phimid=.5*(Phi[:,tstep]+Phi[:,tstep-1]) # \Phi at midpoint
   step_err[tstep]=delt*np.dot(tres,Phimid)#.transpose()
   if tstep<len(time):
      jump_err[tstep]=np.dot(X[:,tstep-1]-X[:,tstep],Phi[:,tstep])
    
     

# Error Contributions
est_err=np.sum(step_err)+np.sum(jump_err)
err=Xref[0]-X[0,-1]

print "Error: ", err
print "Error Estimate: ", est_err

plt.figure(0)
plt.plot(time, X[0,:], time, X[1,:])
plt.savefig("forward_solution.png")

plt.figure(1)
plt.plot(time, Phi[0,:], time, Phi[1,:])
plt.savefig("adjoint_solution.png")


     

