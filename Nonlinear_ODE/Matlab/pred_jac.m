
function jac=pred_jac(X,alpha,beta,tau,gamma);

jac=[alpha-beta*X(2) -beta*X(1) ; tau*X(2) tau*X(1)-gamma];
