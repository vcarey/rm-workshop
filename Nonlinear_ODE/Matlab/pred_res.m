
function res=pred_res(Xcur,Xold,delt,alpha,beta,tau,gamma)

res=Xcur-Xold-delt*[alpha*Xcur(1)-beta*Xcur(1)*Xcur(2); tau*Xcur(1)*Xcur(2)-gamma*Xcur(2)];

sprintf('Current Residual Norm: %f',norm(res))

