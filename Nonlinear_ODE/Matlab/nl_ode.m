
%# Predator-Prey model:  \dot{X}=\begin{array}{ll}{ \alpha & \beta x_2 \\ \gamma x_1 & \lambda \right] X
%# Forward-Model (Backwrd Euler (DG0)) 

%# time mesh points and \delta t
delt=.01;
time=0:delt:10;

%reference solution (backward euler,10000 timesteps)
Xref=[2.000179198587852;1.000810454405660]; %# "truth" X(T=10)

%# solution matrix X(2xtimesteps) for storing solution.
Xsol=zeros(2,length(time));
%# Initial condition X(:,1)
P1=2;
P2=1;
X(:,1)=[P1 ; P2];

%# Model parameters
alpha=1.1;
beta=1;
tau=1;
gamma=1.1;

%# tolerance for newton solve
tol=1E-6;

%# forward timestepping

Xcur=X(:,1);
for tstep=2:length(time)
     Xold=Xcur;
     %# Newton loop
     res=pred_res(Xcur,Xold,delt,alpha,beta,tau,gamma);
     sprintf('Time Step: %d',tstep)
while max(abs(res)) > tol
     %# compute Newton step
     jac=pred_jac(Xcur,alpha,beta,tau,gamma);
     newton=(eye(2)-delt*jac)\-res;
     sprintf('Norm step: %f',norm(newton))
     Xcur=Xcur+newton;
     res=pred_res(Xcur,Xold,delt,alpha,beta,tau,gamma);
  end
%# update and store X at new time value
X(:,tstep)=Xcur;
end

%# Adjoint (Reverse) Solve:Crank-Nicholson(CG1)

Phi=zeros(size(X));
%# Adjoint Initial condition: QoI is Prey population at t=T(final)
Phi(:,length(time))=[1;0];
for tstep=length(time)-1:-1:1
     %# Get Jacobian values at current time interval and assemble RHS
     RHS=Phi(:,tstep+1)+.5*delt*pred_jac(X(:,tstep+1),alpha,beta,tau,gamma)*Phi(:,tstep+1);
     %# Get Jacobian values at new time and build matrix
     AMAT=eye(2)-delt*.5*pred_jac(X(:,tstep),alpha,beta,tau,gamma);
     %# Solve for new time value
     Phi(:,tstep)=AMAT\RHS;
end

%# Error Contributions:
%# No Initial Error (X(0),Phi(0))

%# X is piecewise constant on a time interval and Phi is piecewise linear on a time interval
%# Residual contribution on time interval with midpoint quadrature (-F(X),\Phi)_{T_i}

%# Jump Error from DG=(X(t-)-X(t+),\Phi(2))

step_err(1)=0;
jump_err(1)=0;
for tstep=2:length(time)
   tres=[alpha*X(1,tstep)-beta*X(1,tstep)*X(2,tstep); -tau*X(1,tstep)*X(2,tstep)+gamma*X(2,tstep)];
   Phimid=.5*(Phi(:,tstep)+Phi(:,tstep-1)); %# \Phi at midpoint
   step_err(tstep)=delt*tres'*Phimid;
   if tstep<length(time)
      jump_err(tstep)=(X(:,tstep-1)-X(:,tstep))'*Phi(:,tstep);
   end
end
     
%# Do some plotting
plot(time,X(1,:),time,X(2,:)); %# Approximate Solution

figure;
plot(time,Phi(1,:),time,Phi(2,:)); %# Approximate Adjoint Solution

%# Error Contributions
est_err=sum(step_err)+sum(jump_err)
err=Xref(1)-X(1,end)



     

